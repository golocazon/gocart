package mysql

import (
	"errors"
	"github.com/jinzhu/gorm"
	models "gitlab.com/golocazon/gocart/internal/dbmodels"
)

func NewDBCartRepository(db *gorm.DB) *DBCartRepository {
	return &DBCartRepository{
		DB: db,
	}
}

type DBCartRepository struct {
	DB *gorm.DB
}

func (r *DBCartRepository) Fetch(offset int64, num int64) (carts []*models.Cart, err error) {
	//carts := make([]dbmodels.Cart, 0)
	err = r.DB.Limit(num).Offset(offset).Find(&carts).Error
	return
}

func (r *DBCartRepository) GetByCustomerID(customerID int64) (err error, carts []*models.Cart) {

	err = r.DB.Where("customer_id = ?", customerID).Find(&carts).Error
	return
}

func (r *DBCartRepository) GetByID(cartID int64) (error, *models.Cart) {
	cart := &models.Cart{}
	error := r.DB.First(&cart, cartID).Error
	if error != nil {
		return errors.New("Not found"), cart
	}
	return nil, cart
}

func (r *DBCartRepository) Update(cart *models.Cart) (*models.Cart, error) {
	// Create a new cart
	error := r.DB.Save(&cart).Error
	return cart, error
}

func (r *DBCartRepository) Store(cart *models.Cart) (*models.Cart, error) {
	// Create a new cart
	if cart.CustomerID == 0 {
		return nil, errors.New("Need a customer id for a cart")
	}
	error := r.DB.Create(&cart).Error
	//r.DB.Save(&cart)
	return cart, error
}

func (r *DBCartRepository) Delete(cartID int64) (bool, error) {
	cart := &models.Cart{}
	cart.ID = cartID
	error := r.DB.Delete(&cart).Error
	if error != nil {
		return false, errors.New("Not found")
	}
	return true, nil
}
