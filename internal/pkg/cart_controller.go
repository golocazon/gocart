package pkg

import (
	"gitlab.com/golocazon/gocart/api/models"
)

type CartController interface {
	CreateCart(cart models.Cart) (error, *models.Cart)

	UpdateCartDetails(cartID int64, cart models.Cart) (error, *models.Cart)

	GetCarts(customerID int64) (error, []*models.Cart)

	GetCart(cartID int64) (error, *models.Cart)

	DeleteCart(cartID int64) error

	UpdateCartItem(cartID int64, item models.CartItem) (error, *models.Cart)

	DeleteCartItem(cartID int64, productID int64) (error, *models.Cart)

	EmptyCart(cartID int64) (error, *models.Cart)

	GetCartPrice(cartID int64) (error, float64)
}
