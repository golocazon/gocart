package pkg

import (
	"errors"
	"gitlab.com/golocazon/gocart/api/models"
	"gitlab.com/golocazon/gocart/internal/dbmodels"
)

func NewDefaultCartController(cartRepository CartRepository) *DefaultCartController {
	return &DefaultCartController{
		CartRepository: cartRepository,
	}
}

type DefaultCartController struct {
	CartRepository CartRepository
}

func (r *DefaultCartController) CreateCart(cart models.Cart) (error, *models.Cart) {
	// Create a new cart
	if cart.CustomerID != 0 {
		return errors.New("Need a customer id for a cart"), nil
	}
	dbCart := dbmodels.NewDBCart(&cart)
	createdCart, error := r.CartRepository.Store(dbCart)
	return error, dbmodels.NewAPICart(createdCart)
}

func (r *DefaultCartController) UpdateCartDetails(cartID int64, cart models.Cart) (error, *models.Cart) {
	err, loadedCart := r.CartRepository.GetByID(cartID)
	if err != nil {
		return errors.New("Cart does not exist"), nil
	}
	//TODO: how to ensure the cart belongs to the user?? is it needed?
	mergeCartContent(loadedCart, cart)
	// Create a new cart
	updatedCart, error := r.CartRepository.Update(loadedCart)
	return error, dbmodels.NewAPICart(updatedCart)
}

/*
Merges
*/
func mergeCartContent(origin *dbmodels.Cart, delta models.Cart) {

	origin.Name = delta.Name
}

func (r *DefaultCartController) GetCarts(customerID int64) (err error, carts []*models.Cart) {
	err, dbCarts := r.CartRepository.GetByCustomerID(customerID)
	carts = make([]*models.Cart, len(dbCarts))
	for i, _ := range dbCarts {
		carts[i] = dbmodels.NewAPICart(dbCarts[i])
	}
	return
}

func (r *DefaultCartController) GetCart(cartID int64) (err error, loadedCart *models.Cart) {
	//err, loadedCart =  r.CartRepository.GetByID(cartID)
	return
}

func (r *DefaultCartController) DeleteCart(cartID int64) (err error) {
	_, err = r.CartRepository.Delete(cartID)
	return
}

func (r *DefaultCartController) UpdateCartItem(cartID int64, in models.CartItem) (error, *models.Cart) {
	err, dbCart := r.CartRepository.GetByID(cartID)
	if err != nil {
		return err, nil
	}
	var dbCartItem *dbmodels.CartItem
	for _, item := range dbCart.Items {
		if item.ProductID == in.ProductID {
			dbCartItem = item
		}
	}
	if dbCartItem == nil {
		dbCartItem = dbmodels.NewDBCartItem(&in)
		dbCart.Items = append(dbCart.Items, dbCartItem)
	} else {
		dbCartItem.Quantity = dbCartItem.Quantity + in.Quantity
		removeEmptyItems(dbCart)
	}
	updatedCart, err := r.CartRepository.Update(dbCart)
	if err != nil {
		return err, nil
	}
	return nil, dbmodels.NewAPICart(updatedCart)
}

func removeEmptyItems(cart *dbmodels.Cart) {
	n := 0
	for _, item := range cart.Items {
		if item.Quantity > 0 {
			cart.Items[n] = item
			n++
		}
	}
	cart.Items = cart.Items[:n]
}

func (r *DefaultCartController) DeleteCartItem(cartID int64, productID int64) (error, *models.Cart) {
	err, loadedCart := r.CartRepository.GetByID(cartID)
	if err != nil {
		return err, nil
	}

	for _, item := range loadedCart.Items {
		if item.ProductID == productID {
			item.Quantity = 0
		}
	}
	updatedCart, err := r.CartRepository.Update(loadedCart)
	if err != nil {
		return err, nil
	}
	return nil, dbmodels.NewAPICart(updatedCart)
}

func (r *DefaultCartController) EmptyCart(cartID int64) (error, *models.Cart) {
	err, loadedCart := r.CartRepository.GetByID(cartID)
	if err != nil {
		return err, nil
	}
	loadedCart.Items = make([]*dbmodels.CartItem, 0)
	updatedCart, err := r.CartRepository.Update(loadedCart)
	if err != nil {
		return err, nil
	}
	return nil, dbmodels.NewAPICart(updatedCart)
}

func (r *DefaultCartController) GetCartPrice(cartID int64) (error, float64) {
	//err, cart := r.GetCart(cartID)
	err, _ := r.GetCart(cartID)
	if err != nil {
		return err, 0
	}
	/*
		for _,item := range cart.Items {
			product := getProduct(item.ProductID)
			unitPrice = product.getUnitPrice() * item.Quantity

		}
		return nil, cart.GetPrice()
	*/

	return nil, 0
}
