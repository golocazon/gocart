package pkg

import (
	"gitlab.com/golocazon/gocart/internal/dbmodels"
)

type CartRepository interface {
	Fetch(offset int64, num int64) ([]*dbmodels.Cart, error)
	GetByCustomerID(customerID int64) (error, []*dbmodels.Cart)
	GetByID(cartID int64) (error, *dbmodels.Cart)
	Update(cart *dbmodels.Cart) (*dbmodels.Cart, error)
	Store(a *dbmodels.Cart) (*dbmodels.Cart, error)
	Delete(id int64) (bool, error)
}
