package dbmodels

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/golocazon/gocart/api/models"
)

type Cart struct {
	gorm.Model
	ID         int64       `json:"id"`
	Name       string      `json:"name"`
	CustomerID int64       `json:"customer_id",gorm:"not null"`
	Items      []*CartItem `json:"items"`
}

type CartItem struct {
	gorm.Model
	CartRefer int64 `json:"-"`
	ProductID int64 `json:"product_id"`
	Quantity  int64 `json:"quantity"`
}

func NewAPICart(cart *Cart) *models.Cart {
	newCart := models.Cart{
		ID:         cart.ID,
		Name:       cart.Name,
		CustomerID: cart.CustomerID,
		Items:      make([]models.CartItem, len(cart.Items)),
	}
	for i, _ := range cart.Items {
		newCart.Items[i] = *newCartItem(cart.Items[i])
	}
	return &newCart
}

func NewDBCart(cart *models.Cart) *Cart {
	newCart := Cart{
		ID:         cart.ID,
		Name:       cart.Name,
		CustomerID: cart.CustomerID,
		Items:      make([]*CartItem, len(cart.Items)),
	}
	for i, _ := range cart.Items {
		newCart.Items[i] = NewDBCartItem(&cart.Items[i])
	}
	return &newCart
}

func NewDBCartItem(item *models.CartItem) *CartItem {
	return &CartItem{
		ProductID: item.ProductID,
		Quantity:  item.Quantity,
	}
}

func newCartItem(item *CartItem) *models.CartItem {
	return &models.CartItem{
		ProductID: item.ProductID,
		Quantity:  item.Quantity,
	}
}
