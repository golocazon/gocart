package internal

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/golocazon/gocart/api/services"
	api2 "gitlab.com/golocazon/gowsfwk/api"

	//"github.com/go-chi/render"
	"gitlab.com/golocazon/gocart/api/models"
	"gitlab.com/golocazon/gocart/internal/pkg"
	"net/http"
	"strconv"
)

type CartService interface {
	Router(r chi.Router)
}

type DefaultCartService struct {
	Controller pkg.CartController
}

func NewCartService(controller pkg.CartController) CartService {
	return &DefaultCartService{
		Controller: controller,
	}
}

func (c *DefaultCartService) Router(r chi.Router) {
	r.Route("/cart", func(r chi.Router) {
		r.Get("/{id}", c.getCart())
		r.Put("/{id}", c.updateCart)
		//r.Post("/", c.createCart)
	})
	r.Get("/carts", c.getCarts)
	r.Post("/cart", c.createCart)
	r.Get("/user/{userId}/carts", c.getCarts)
	r.Delete("/user/cart/{id}", c.deleteCart())
	r.Patch("/user/cart/{id}/item/{itemId}", c.updateCartItem)
	r.Delete("/user/cart/{id}/item/{itemId}", c.deleteCartItem)
	r.Delete("/user/cart/{id}/items", c.emptyCart)
	r.Get("/user/cart/{id}/price", c.getCartPrice)

}

type CartInput struct {
	ID    int64
	name  string
	items []*CartItemInput
}

type CartItemInput struct {
	name string
}

type GetCartRequest struct {
}

type ResponseError struct {
	description string
}

type Response struct {
	code  int
	error *ResponseError
}

type GetCartResponse struct {
	Response
	cart CartInput
}

func (s *DefaultCartService) getCart() http.HandlerFunc {
	type request struct {
		Name string
	}
	type response struct {
		Greeting string `json:"greeting"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		cartID, err := strconv.Atoi(val)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		err, createdCart := s.Controller.GetCart(int64(cartID))
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartResponse{Cart: *createdCart})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

type CreateCartRequest struct {
	cart CartInput
}

type CreateCartResponse struct {
	cart CartInput
}

func (c *DefaultCartService) createCart(w http.ResponseWriter, r *http.Request) {
	userID, err := getUserId(r)
	var cart models.Cart
	err = json.NewDecoder(r.Body).Decode(&cart)
	cart.CustomerID = userID
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	err, createdCart := c.Controller.CreateCart(cart)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}

	js, err := json.Marshal(&services.CartResponse{Cart: *createdCart})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

type UpdateCartRequest struct {
	cart CartInput
}

type UpdateCartResponse struct {
	cart CartInput
}

func (c *DefaultCartService) updateCart(w http.ResponseWriter, r *http.Request) {
	userID, err := getUserId(r)
	val := chi.URLParam(r, "id")
	cartID, err := strconv.Atoi(val)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	var updateCart models.Cart
	err = json.NewDecoder(r.Body).Decode(&updateCart)
	err, cart := c.Controller.UpdateCartDetails(int64(cartID), updateCart)
	//c.Controller.UpdateCartItem()
	err = json.NewDecoder(r.Body).Decode(&cart)
	cart.CustomerID = userID
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	//cart.
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	/*
		js, err := json.Marshal(&UpdateCartResponse{cart: &CartInput{}})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	*/
	w.Header().Set("Content-Type", "application/json")
	//w.Write(js)

}

func NewCartInput(cart *models.Cart) *CartInput {
	cartInput := &CartInput{ID: cart.ID, name: cart.Name}

	return cartInput
}
func getUserId(r *http.Request) (userID int64, err error) {
	userIDVal := chi.URLParam(r, "userId")
	if len(userIDVal) > 0 {
		userIDAsInt, _ := strconv.Atoi(userIDVal)
		userID = int64(userIDAsInt)
	} else {
		_, claims, _ := jwtauth.FromContext(r.Context())
		userIDAsFloat64 := claims["user_id"].(float64)
		userID = int64(userIDAsFloat64)
	}
	return
}

func (c *DefaultCartService) getCarts(w http.ResponseWriter, r *http.Request) {

	userID, _ := getUserId(r)

	err, carts := c.Controller.GetCarts(userID)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &CartsResponse{ Carts: &carts,})
	js, err := json.Marshal(&api2.Response{
		Content: services.CartsResponse{Carts: carts},
		Error:   nil,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func (s *DefaultCartService) deleteCart() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		cartID, err := strconv.Atoi(val)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		err = s.Controller.DeleteCart(int64(cartID))
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartResponse{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func (c *DefaultCartService) updateCartItem() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		cartID, err := strconv.Atoi(val)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		var updateCartItem models.CartItem
		err = json.NewDecoder(r.Body).Decode(&updateCartItem)
		err, cart := c.Controller.UpdateCartItem(int64(cartID), updateCartItem)

		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartResponse{
			Cart: *cart,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func (c *DefaultCartService) deleteCartItem() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		val2 := chi.URLParam(r, "itemId")
		cartID, err := strconv.Atoi(val)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		productID, err := strconv.Atoi(val2)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		err, cart := c.Controller.DeleteCartItem(int64(cartID), int64(productID))

		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartResponse{
			Cart: *cart,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func (c *DefaultCartService) emptyCart() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		cartID, err := strconv.Atoi(val)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		err, cart := c.Controller.EmptyCart(int64(cartID))

		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartResponse{
			Cart: *cart,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func (c *DefaultCartService) getCartPrice() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		cartID, err := strconv.Atoi(val)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		err, price := c.Controller.GetCartPrice(int64(cartID))

		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.CartPriceResponse{
			Price: price,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}
