package Mock

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/golocazon/gocart/api/models"
)

func NewMockCartController() *MockCartController {
	return &MockCartController{
		cartById:     make(map[uint]*models.Cart),
		cartItemById: make(map[uint]*models.CartItem),
	}
}

type MockCartController struct {
	cartById     map[uint]*models.Cart
	cartItemById map[uint]*models.CartItem
	counter      uint
}

func (mcc *MockCartController) nextId() uint {
	mcc.counter = mcc.counter + 1
	return mcc.counter
}

func (r *MockCartController) CreateCart(customerID uint) (error, *models.Cart) {
	// Create a new cart
	cart := &models.Cart{
		CustomerID: customerID,
		Model: gorm.Model{
			ID: r.nextId(),
		},
	}
	return nil, cart
}

func (r *MockCartController) CreateCartItem(item models.CartItem) (error, *models.CartItem) {
	if item.ID != 0 {
		return errors.New("CartItem already created"), nil
	}
	// Create a new cart
	item.ID = r.nextId()
	r.cartItemById[item.ID] = &item
	return nil, &item
}

func (r *MockCartController) GetCart(cartID uint) (error, *models.Cart) {
	cart, err := r.cartById[cartID]
	if !err {
		return errors.New("Not found"), cart
	}
	return nil, cart
}

func (r *MockCartController) AddCartItem(cartID uint, item models.CartItem) (error, *models.CartItem) {
	err, cart := r.GetCart(cartID)
	if err != nil {
		return err, nil
	}
	if item.ID != 0 {
		return errors.New("Already assigned to a cart"), nil
	}
	err, newItem := r.CreateCartItem(item)
	if err != nil {
		return err, nil
	}
	cart.Items = append(cart.Items, *newItem)
	return nil, newItem
}

func (r *MockCartController) UpdateCartItem(cartItemID uint, item models.CartItem) error {
	return nil
}

func (r *MockCartController) RemoveCartItem(cartItemID uint) error {
	return nil
}

func (r *MockCartController) EmptyCart(cartID uint) error {
	err, cart := r.GetCart(cartID)
	if err != nil {
		return err
	}
	cart.Items = make([]models.CartItem, 0)
	return nil
}

func (r *MockCartController) GetCartPrice(cartID uint) (error, float64) {
	err, cart := r.GetCart(cartID)
	if err != nil {
		return err, 0
	}
	return nil, cart.GetPrice()
}
