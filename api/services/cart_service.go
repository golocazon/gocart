package services

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/golocazon/gocart/api/models"
	"gitlab.com/golocazon/gocart/internal/pkg"
	"gitlab.com/golocazon/gologger"
	api2 "gitlab.com/golocazon/gowsfwk/api"
)

type CartService interface {
	pkg.CartController
}

type CartServiceClient struct {
	Client *api2.Client
}

func NewCartServiceClient(client *api2.Client) *CartServiceClient {
	return &CartServiceClient{
		Client: client,
	}
}

func (c *CartServiceClient) CreateCart(cart models.Cart) (error, *models.Cart) {

	req, err := c.Client.NewRequest("POST", "/api/cart", cart)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return err, &cartResponse.Cart
}

func (c *CartServiceClient) UpdateCartDetails(cartID int64, cart models.Cart) (error, *models.Cart) {
	req, err := c.Client.NewRequest("PUT", "/api/cart/"+string(cartID), cart)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return err, &cartResponse.Cart
}

func (c *CartServiceClient) GetCarts(customerID int64) (error, []*models.Cart) {
	gologger.Infof("Called GetCarts with customer id %v", customerID)
	req, err := c.Client.NewRequest("GET", "/api/user/"+string(customerID)+"/carts", nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var cartsResponse CartsResponse
	mapstructure.Decode(response.Content, &cartsResponse)
	return err, cartsResponse.Carts
}

func (c *CartServiceClient) GetCart(cartID int64) (error, *models.Cart) {
	gologger.Infof("Called GetCart  with cart id %v", cartID)
	req, err := c.Client.NewRequest("GET", "/api/cart/"+string(cartID), nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return err, &cartResponse.Cart
}

func (c *CartServiceClient) DeleteCart(cartID int64) error {
	gologger.Infof("Delete GetCarts with cart id %v", cartID)
	req, err := c.Client.NewRequest("DELETE", "/api/cart/"+string(cartID), nil)
	if err != nil {
		return err
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	return err
}

func (c *CartServiceClient) UpdateCartItem(cartID int64, item models.CartItem) (error, *models.Cart) {
	gologger.Infof("UpdateCartItem with cart id %v", cartID)
	req, err := c.Client.NewRequest("PATCH", "/api/cart/"+string(cartID)+"/item", item)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	if err != nil {
		return err, nil
	}
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return nil, &cartResponse.Cart
}

func (c *CartServiceClient) DeleteCartItem(cartID int64, productID int64) (error, *models.Cart) {
	gologger.Infof("UpdateCartItem with cart id %v", cartID)
	req, err := c.Client.NewRequest("DELETE", "/api/cart/"+string(cartID)+"/item/"+string(productID), nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	if err != nil {
		return err, nil
	}
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return nil, &cartResponse.Cart
}

func (c *CartServiceClient) EmptyCart(cartID int64) (error, *models.Cart) {
	gologger.Infof("UpdateCartItem with cart id %v", cartID)
	req, err := c.Client.NewRequest("DELETE", "/api/cart/"+string(cartID)+"/items", nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	if err != nil {
		return err, nil
	}
	var cartResponse CartResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return nil, &cartResponse.Cart
}

func (c *CartServiceClient) GetCartPrice(cartID int64) (error, float64) {
	gologger.Infof("UpdateCartItem with cart id %v", cartID)
	req, err := c.Client.NewRequest("GET", "/api/cart/"+string(cartID)+"/price", nil)
	if err != nil {
		return err, 0
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	if err != nil {
		return err, 0
	}
	var cartResponse CartPriceResponse
	mapstructure.Decode(response.Content, &cartResponse)
	return nil, cartResponse.Price
}

type CartsResponse struct {
	api2.Response
	Carts []*models.Cart `json:"carts"`
}

type CartResponse struct {
	api2.Response
	Cart models.Cart `json:"cart"`
}

type CartPriceResponse struct {
	api2.Response
	Price float64
}
