package models

type Cart struct {
	ID         int64      `json:"id"`
	CustomerID int64      `json:"customerId"`
	Name       string     `json:"name"`
	Items      []CartItem `json:"items"`
}

type CartItem struct {
	Quantity  int64 `json:"quantity"`
	ProductID int64 `json:"productID"`
}
